import React, { Component } from 'react';

import './css/CitySearch.css'


class CitySearch extends Component {
  state={
    value: 'Kyiv',
  }

  handleSubmit = (city) =>{
    this.props.handleCitySubmit(city);
    this.setState({value: ''});
  }
  hadleType = (e) => {
    this.setState({value: e.target.value})
  }
  render(){
    const butCol = {
      backgroundColor: this.props.day.bgcol,
    }
    return (
      <div
        className='logo-city-search-wrapper'>
        <div
          className='city-search-wrapper'
          >
            <input
              placeholder='enter city..'
              value={this.state.value}
              onChange={this.hadleType}
            />
            <button
              style={butCol}
              onClick={() => this.handleSubmit(this.state.value)}
              >
                {/* <i className="fa fa-arrow-right"></i>*/}
                Submit</button>
          </div>
      </div>
    );
  }

}
export default CitySearch;
