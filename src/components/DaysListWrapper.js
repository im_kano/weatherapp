import React, { Component } from 'react';
import './css/DaysListWrapper.css';

import SingleDay from './SingleDay.js';

class DaysListWrapper extends Component {

  handleDayClick = (id) => {
    this.props.handleDayClick(id);
  };

  render(){
    const days = this.props.days.map((d,index) => {
      return (
        <SingleDay
          key={index}
          dayObj={d}
          handleDayClick={this.handleDayClick}
        />
      );
    });
    return (
      <div className='days-list-wrapper'>
        {days}
      </div>
    );
  }

}
export default DaysListWrapper;
