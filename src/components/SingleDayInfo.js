import React, { Component } from 'react';
import './css/SingleDayInfo.css';

class SingleDayInfo extends Component {

  render(){
    const curtainObj = this.props.day;
    const bgCol = {
      backgroundColor: curtainObj.bgcol,
    }
    return (
      <div
        className={curtainObj.active ? 'info-wrap is-active' : 'info-wrap'}
        id='info'
        style={bgCol}
        >

      </div>
    );
  }
}
export default SingleDayInfo;
