import React, { Component } from 'react';
import './css/SingleDay.css';
import './css/weather-icons.min.css';

import backLogo from './img/sunny.svg';

class SingleDay extends Component {
  handleDayClick = (id) => {
    this.props.handleDayClick(id);
  }
  render(){

    const dayObj = this.props.dayObj;
    const bgCol = {
      backgroundColor: dayObj.bgcol,
    };


    return (
      <div
        className= {dayObj.active ? 'single-day is-active-day':'single-day'}
        style={bgCol}
        onClick={() => this.handleDayClick(dayObj.id)}
        >
        <div><h1>{dayObj.day}</h1></div>
        <div className='weather-logo'>
          <i className='wi wi-day-sunny'></i>
        </div>
        <div className='temp-range'>
          <div className='temp'><span>{dayObj.conds.mintemp}</span></div>
          <div className='temp'><span><i className='wi wi-thermometer'></i></span></div>
          <div className='temp'><span>{dayObj.conds.maxtemp}</span></div>
        </div>
        <div className='logo-svg-back'>
          {/* <img src={backLogo} alt='back-logo'/> */}
        </div>
      </div>
    );
  }
}
export default SingleDay;
