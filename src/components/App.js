import React, { Component } from 'react';
import './css/App.css';

import uuid from 'uuid';


import DaysListWrapper from './DaysListWrapper.js';
import SingleDayInfo from './SingleDayInfo.js';
import CitySearch from './CitySearch.js';

const API_KEY = "4b5c7388a23082e49d2bf0bcb7c3748a";

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      activeDay: 'zxc',
      isClicked: false,
      days: [
        {
          id: 'zxc',
          active: true,
          day: 'Monday',
          dayind: 1,
          conds: {
            mintemp: 10,
            maxtemp: 22,
          },
          pic: '',
          bgcol: '#ff6348',
        },
        {
          id: uuid.v4(),
          active: false,
          day: 'Tuesday',
          dayind: 2,
          conds: {
            mintemp: 10,
            maxtemp: 22,
          },
          pic: '',
          bgcol: '#5352ed',
        },
        {
          id: uuid.v4(),
          active: false,
          day: 'Wednesday',
          dayind: 3,
          conds: {
            mintemp: 10,
            maxtemp: 22,
          },
          pic: '',
          bgcol: '#ffa502',
        },
        {
          id: uuid.v4(),
          active: false,
          day: 'Thursday',
          dayind: 4,
          conds: {
            mintemp: 10,
            maxtemp: 22,
          },
          pic: '',
          bgcol: '#2ed573',
        },
        {
          id: uuid.v4(),
          active: false,
          day: 'Friday',
          dayind: 5,
          conds: {
            mintemp: 10,
            maxtemp: 22,
          },
          pic: '',
          bgcol: '#ff7f50',
        },
      ],
    };

  };

  inpDataParse = (inparr) => {
    let daout = [];
    let dain = [];
    let k = 0;
    for(let i=0; i<inparr.length; i++){
      if(new Date(inparr[i].dt_txt).getHours() === 0){
        daout[k] = dain;
        k++;
        dain=[];
        dain.push(inparr[i]);
      }else{
        dain.push(inparr[i]);
      }
  };
  return daout;
};

  getWeather = async (city) =>{
    const dayNum = new Date().getDay();
    const activeDayInd = this.state.days[0].id;
    const weekdays = ['Sunday','Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    // const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city},ua&mode=json&appid=${API_KEY}&units=metric`);
    const api_call = await fetch(`http://api.openweathermap.org/data/2.5/forecast?q=${city},ua&mode=json&appid=${API_KEY}&units=metric`);
    const data = await api_call.json();
    let dataAr = [...data.list];
    const alldays = this.inpDataParse(dataAr);
    let i = 0;
    let ar = [];
    const alldaysparsed = alldays.map((ad) => {
      // console.log(ad[i].main.temp_min);
      if(ad.length !== 0){
        ar.push(ad[i]);
      }
      i++;
    });
//     console.log(alldays, 'ad');
//
//     console.log(ar, 'ar');
//
//     let k = 0;
//     ar.map((d) => {
//       console.log(d);
//       if (k == 0) {
//         console.log(k);
//         k++;
//         this.setState({
//
//           days: [].concat({
//             id: uuid.v4(),
//             active: true,
//             day: weekdays[new Date(d.dt_txt).getDay()],
//             dayind: new Date(d.dt_txt).getDay(),
//             conds: {
//               mintemp: d.main.temp_min,
//               maxtemp: d.main.temp_max,
//             },
//             pic: '',
//             bgcol: '#ff7f50',
//           })
//
//         });
//
//       }
//        else {
//
//       k++;
//       console.log(k);
//
//       this.setState({
//
//         days: this.state.days.concat({
//
//           id: uuid.v4(),
//           active:false,
//           day: weekdays[new Date(d.dt_txt).getDay()],
//           dayind: new Date(d.dt_txt).getDay(),
//           conds: {
//             mintemp: d.main.temp_min,
//             maxtemp: d.main.temp_max,
//           },
//           pic: '',
//           bgcol: '#ff7f50',
//
//         }),
//
//       });
//
//     };
//
//   });
//
//   this.setState({
//     activeDay: this.state.days[0].id,
//   });
// console.log(this.state, 'state');

// this.setState({
//   days: [].
// })

  let colorsar = [
    '#ff7f50',
    '#2ed573',
    '#ffa502',
    '#5352ed',
    '#ff6348'
  ];


    let newState = ar.map( (day)=>({
      id: uuid.v4(),
      active: ar.indexOf(day) == 0 ? true : false,
      day: weekdays[new Date(day.dt_txt).getDay()],
      dayind: new Date(day.dt_txt).getDay(),
      conds: {
        mintemp: day.main.temp_min,
        maxtemp: day.main.temp_max,
      },
      pic: '',
      bgcol: colorsar[ar.indexOf(day)],
    }) )

  this.setState({
    activeDay: newState[0].id,
    days: newState,
  })

//
//
//     this.setState({
//       days: this.state.days.map((day) => {
//         Object.assign({}, day, {
//         id: uuid.v4(),
//         active: k == 0 ? true : false,
//         day: weekdays[new Date(ar[k].dt_txt).getDay()],
//         dayind: new Date(ar[k].dt_txt).getDay(),
//         conds: Object.assign({}, day.conds, {
//           mintemp: ar[k].main.temp_min,
//           maxtemp: ar[k].main.temp_max,
//         })
//       })
//       k++;
//     }),
// });

//     this.setState({
//       days: this.state.days.map((day) => {
//         Object.assign({}, day, {
//         id: uuid.v4(),
//         active: k == 0 ? true : false,
//         day: weekdays[new Date(ar[k].dt_txt).getDay()],
//         dayind: new Date(ar[k].dt_txt).getDay(),
//         conds: Object.assign({}, day.conds, {
//           mintemp: ar[k].main.temp_min,
//           maxtemp: ar[k].main.temp_max,
//         })
//       })
//       k++;
//     }),
// });
  this.setState({
    activeDay: this.state.days[0].id,
  });
    // console.log(this.state.days);
    console.log(ar);
    // const dataMap = data.list.map((d) => {
    //   console.log( weekdays[new Date(d.dt_txt).getDay()] + new Date(d.dt_txt).getDay() );
    // })
    // console.log('today is: ' + weekdays[new Date("2018-10-02 18:00:00").getDay()] );
    // console.log(data);
    // const date = new Date('2018-09-29 18:00:00');
    // console.log(date.getDay());
  }

  handleCitySubmit = (city) => {
    this.getWeather(city);
  };

//   componentWillMount(){
//     const date =  new Date();
//     const day = date.getDay();
//     const currentDayId = this.state.days[day].id;
//     const currentActiveVal = this.state.days[day].active;
//     this.setState({
//       activeDay: this.state.days[day].id,
//       days: this.state.days.map((day) => (day.id === currentDayId ? Object.assign({}, day, { active: !currentActiveVal }) : day))
//     })
// };



  handleDayClick = (index) => {
    const actDayId = this.state.activeDay;
    const daysAr = this.state.days;
    const actDay = daysAr.find((d) => d.id === index);
    let changeVal = actDay.active;
    this.setState({
          activeDay: index,
          days: this.state.days.map((day) => (day.id === index ? Object.assign({}, day, { active: !changeVal }) : Object.assign({}, day, { active: false })))
    });

  };

  render() {
    const date =  new Date().toDateString();
    const activeDay = this.state.activeDay;
    const daysArr = this.state.days;
    console.log(daysArr);
    const objToInf = daysArr.find((d) => d.id == activeDay );
    const objToInfInd = daysArr.findIndex((d) => d.id === activeDay );

    return (
      <div className="App">
        <div className='app-header'>Today is: <span>{date}</span> </div>
        <CitySearch
          day={objToInf}
          activeDayInd={objToInfInd}
          handleCitySubmit={this.handleCitySubmit}
        />
        <DaysListWrapper
          days={daysArr}
          handleDayClick={this.handleDayClick}
      />
        <SingleDayInfo
          day = {objToInf}
          isClicked={this.state.isClicked}
        />
      </div>

    );
  }
}

export default App;
